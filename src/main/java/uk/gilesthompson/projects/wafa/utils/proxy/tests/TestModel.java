/*
 * Copyright 2019 Giles Thompson.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package uk.gilesthompson.projects.wafa.utils.proxy.tests;

/**
 *
 * @author giles
 */
public class TestModel {
    
    private int id;
    
    private String firstName;
     
    private String surname;
    
    private float height;
    
    private byte timesBeen;
    
    private short timesFamilyBeen;
    
    private char maleOrFemale;
    
    private boolean alreadyRegistered;
    
    private double percentageOfFans;
    
    private long pointsCollected;

  
    public int getId() {
        return id;
    }

    public void setId(int id) {
        
        this.printDebugMessage("Source Class instance successfully received call to setId method");
        this.id = id;
    }
    
    public String getFirstName() {
        this.printDebugMessage("Source Class instance successfully received call to getId method");
        return firstName;
    }

    
    public void setFirstName(String firstName) {
        this.printDebugMessage("Source Class instance successfully received call to setFirstname method");
        this.firstName = firstName;
    }
    

    public String getSurname() {
        this.printDebugMessage("Source Class instance successfully received call to getSurname method");
        return surname;
    }

    
    public void setSurname(String surname) {
        this.printDebugMessage("Source Class instance successfully received call to setSurname method");
        this.surname = surname;
    }
    

   
    public float getHeight() {
        this.printDebugMessage("Source Class instance successfully received call to getHeight method");
        return height;
    }

    public void setHeight(float height) {
        
        this.printDebugMessage("Source Class instance successfully received call to setHeight method");
        this.height = height;
    }

    public byte getTimesBeen() {
        this.printDebugMessage("Source Class instance successfully received call to getTimesBeen method");
        return timesBeen;
    }

    public void setTimesBeen(byte timesBeen) {
        this.printDebugMessage("Source Class instance successfully received call to setTimesBeen method");
        this.timesBeen = timesBeen;
    }

    public short getTimesFamilyBeen() {
        this.printDebugMessage("Source Class instance successfully received call to getTimesFamilyBeen method");
        return timesFamilyBeen;
    }

    public void setTimesFamilyBeen(short timesFamilyBeen) {
        this.printDebugMessage("Source Class instance successfully received call to setTimesFamilyBeen method");
        this.timesFamilyBeen = timesFamilyBeen;
    }

    public char getMaleOrFemale() {
        this.printDebugMessage("Source Class instance successfully received call to getMaleOrFemale method");
        return maleOrFemale;
    }

    public void setMaleOrFemale(char maleOrFemale) {
        this.printDebugMessage("Source Class instance successfully received call to setMaleOrFemale method");
        this.maleOrFemale = maleOrFemale;
    }

    public boolean isAlreadyRegistered() {
        this.printDebugMessage("Source Class instance successfully received call to isAlreadyRegistered method");
        return alreadyRegistered;
    }

    public void setAlreadyRegistered(boolean alreadyRegistered) {
        this.printDebugMessage("Source Class instance successfully received call to setAlreadyRegistered method");
        this.alreadyRegistered = alreadyRegistered;
    }

   
    public double getPercentageOfFans() {
        this.printDebugMessage("Source Class instance successfully received call to getPercentageOfFans method");
        return percentageOfFans;
    }

    public void setPercentageOfFans(double percentageOfFans) {
        this.printDebugMessage("Source Class instance successfully received call to setPercentageOfFans method");
        this.percentageOfFans = percentageOfFans;
    }
    
    public long getPointsCollected() {
        this.printDebugMessage("Source Class instance successfully received call to getPointsCollected method");
        return pointsCollected;
    }

    public void setPointsCollected(long pointsCollected) {
        this.printDebugMessage("Source Class instance successfully received call to setPointsCollected method");
        this.pointsCollected = pointsCollected;
    }

    private void printDebugMessage(String message) {
        
        System.out.println(message);
    }
   
   
    
}
