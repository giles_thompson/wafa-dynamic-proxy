/*
 * Copyright 2019 Giles Thompson.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package uk.gilesthompson.projects.wafa.utils.proxy;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import uk.gilesthompson.projects.wafa.utils.proxy.exceptions.ProxyGenerationException;
import uk.gilesthompson.projects.wafa.utils.proxy.gen.ProxyByteCodeGenerator;
import uk.gilesthompson.projects.wafa.utils.proxy.utilities.ProxyHandler;

/**
 * DynamicProxyFactory is a proxy generation utility that is able to 
 * dynamically generate a proxy for any arbitrtarily specified, standards
 * compliant java class at runtime.<br>
 * 
 * Where "additional interfaces" are specified the generated DynamicProxy will also 
 * provide an implementation for the methods defined and these interfaces in all cases
 * calls made against the public interface methods of the DynamicProxy will always be
 * delegated to the supplied ProxyHandler instance.<br>
 * 
 * NB: DynamicProxyFactory may be considered thread-safe for the majority of practical
 *     usecases and thus a single DynamicProxyFactory may be used to generate numerous
 *     DynamicProxy implementations simultaneously.
 * 
 * Notice: DynamicProxyFactory is part of the WAFA (Webservices Application Framework API) project.
 *  
 * @author giles
 */
public abstract class DynamicProxyFactory {
    
    /**
     * Creates a new DynamicProxyFactory instance.
     * @return DynamicProxyFactory A new DynamicProxyFactory
     *                             implementation instance.
    */
    public static DynamicProxyFactory create(){
        
        return new DefaultDynamicProxyFactory();
    }
    
    /**
     * Dynamically creates a new DynamicProxy for the specified original class instance.The resulting
     * proxy will implement all methods defined in the public interface of the original
     * class as well as those present in the array of "additional interfaces" (where specified)
     * as such the proxy class may be safely cast to the concrete type of the original 
     * class without a ClassCastException being raised.
     * 
     * @param originalClassInst An instance of the original class to be proxied.
     * @param aHandler Defines how proxied methods should be handled when called. 
     * @param additionalInterfaces Used to add additional methods (over and above those
     *                             defined in the original class) to the proxy.
     * @return DynamicProxy A new dynamic proxy instance. 
     * @throws ProxyGenerationException Where one or more errors occur during the generation of the DynamicProxy. 
     */
    public abstract DynamicProxy newDynamicProxy(Object originalClassInst,ProxyHandler aHandler,Class... additionalInterfaces) throws ProxyGenerationException;
    
    /**
     * Returns reference to the ClassLoader internally used by
     * the DynamicProxyFactory to load the generated proxy classes.
     * @return Class
     */
    public abstract ClassLoader getInternalClassLoader();
    
    /**
     * Clears this DynamicProxyFactory instance's loaded DynamicProxy 
     * class cache, thereby allowing the same class to be redifined multiple
     * times over the course of the execution of the current JVM process.<br>
     * Internally the method destroys the factory's current internal ClassLoader 
     * instance (and all constituent loaded classes its managing) before proceeding 
     * to create a new ClassLoader instance.<br><br>
     * 
     * Please note that where this method is NOT called a DynamicProxy may only 
     * be defined ONCE over the course of a single JVM process; calling the
     * "newDynamicProxy" method more than once for a single class will result in
     * the SAME class definition being returned.
     */
    public abstract void clearCache();
    
    
   
    
   
    
    /** Default DynamicProxyFactory implementation. */
    public static final class DefaultDynamicProxyFactory extends DynamicProxyFactory{
        
        //private ProxyByteCodeGenerator proxyByteCodeGenerator;
        
        private DynamicProxyClassLoader dynamicProxyClassLoader;
        
        public DefaultDynamicProxyFactory() {
            
            this.dynamicProxyClassLoader = DynamicProxyClassLoader.newInst(Thread.currentThread()
                                                                                 .getContextClassLoader());
        }
       

        @Override
        public DynamicProxy newDynamicProxy(Object originalClassInst, ProxyHandler aHandler, Class... additionalInterfaces) throws ProxyGenerationException {
            
            try {
                
                //if the class ALREADY exist in our ClassLoader cache we pass in null as this
                //will signal the "generateDynamicProxyClass" method to use the existing class definition.
                byte[] classDefBytes = (this.dynamicProxyClassLoader.existsInCache(originalClassInst.getClass().getName()))? null : this.generateDynamicProxyClass(originalClassInst, additionalInterfaces);
                
                return (DynamicProxy)this.createDynamicProxyClassInstance(classDefBytes,
                                                                          originalClassInst,
                                                                          aHandler,
                                                                          additionalInterfaces);
            } 
            catch (IOException | NoSuchMethodException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
                Logger.getLogger(DynamicProxyFactory.class.getName()).log(Level.SEVERE, null, ex);
                throw new ProxyGenerationException("An error occured during the process of generating the DynamicProxy instance.");
            }
        }

        @Override
        public ClassLoader getInternalClassLoader() {
            
            return this.dynamicProxyClassLoader;
        }
        
        
        private byte[] generateDynamicProxyClass(Object originalClassInst,Class... additionalInterfaces) throws IOException{
            
            return ProxyByteCodeGenerator.create(originalClassInst.getClass().getName(),additionalInterfaces)
                                         .generate();
            

            
        }
        
        private Object createDynamicProxyClassInstance(byte[] dynamicProxyByteCodeArr,
                                                       Object originalClassInst, 
                                                       ProxyHandler aHandler, 
                                                       Class... additionalInterfaces) throws NoSuchMethodException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException{
            
                
         
                //if a null byte array was passed in then this indicates that we have
                //already loaded the class and thus we may create an instance of it 
                //The class loader will return the instance from the cache...
                Class dynamicProxyClass = dynamicProxyClass = this.dynamicProxyClassLoader
                                                                  .loadClassBytes(dynamicProxyByteCodeArr,
                                                                                  originalClassInst.getClass()
                                                                                                   .getName());
                
                //create an instance of the class and return it.
                Object proxyInstance = dynamicProxyClass.getConstructor(new Class[]{Object.class,ProxyHandler.class,new Class[]{}.getClass()})
                                                        .newInstance(new Object[]{originalClassInst,
                                                                                  aHandler,
                                                                                  additionalInterfaces});
                
                
                return proxyInstance;
           
        }

        @Override
        public void clearCache() {
            
            this.dynamicProxyClassLoader = null;
            this.dynamicProxyClassLoader = DynamicProxyClassLoader.newInst(Thread.currentThread()
                                                                                 .getContextClassLoader());
            
        }
       
        
  private static final class DynamicProxyClassLoader extends ClassLoader{
            
         private final Map<String,Class> loadedClassesMap;
            
         public static DynamicProxyClassLoader newInst(ClassLoader parent){
            
            return new DynamicProxyClassLoader(parent);
         }
        
        DynamicProxyClassLoader(ClassLoader parent){
            
            super(parent);
            this.loadedClassesMap = new HashMap<>();
        }
      
        public Class loadClassBytes(byte[] classBytes,String className){
            
            Class loadedClass;
            
            synchronized(this.loadedClassesMap){
                
                loadedClass = this.loadedClassesMap.get(className);
                
            }
            
            if(loadedClass == null){
              
                loadedClass = defineClass(null,classBytes, 0, classBytes.length);
         
                
                synchronized(this.loadedClassesMap){
                
                     this.loadedClassesMap.put(className,loadedClass);
                     
                     //add proxy class name to so that request to load the proxy variant
                     //as part of the classloader resoultion mechanism STILL 
                     //returns reference to the generated class.
                     this.loadedClassesMap.put(className.concat("Proxy"), loadedClass);
                     
                    
                }
            }
            
            
            return loadedClass;
        }
        
        public boolean existsInCache(String className){
            
            return this.loadedClassesMap.containsKey(className);
        }
        

       @Override
       protected Class<?> findClass(String name) throws ClassNotFoundException {
           Class existingClass;  
           if((existingClass = this.loadedClassesMap.get(name)) != null)
                return existingClass;
           
           return super.findClass(name);
       }
        
        

        }
    }
}
