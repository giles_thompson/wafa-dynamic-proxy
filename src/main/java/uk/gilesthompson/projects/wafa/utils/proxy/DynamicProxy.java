/*
 * Copyright 2019 Giles Thompson.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package uk.gilesthompson.projects.wafa.utils.proxy;

import uk.gilesthompson.projects.wafa.utils.proxy.utilities.ProxyHandler;

/**
 * DynamicProxy implementations are returned from the DynamicProxyFactory as this
 * abstract type which amongst other things, provides a convenient method by which 
 * further data pertaining to the proxy may be obtained including the details of the 
 * main class this DynamicProxy instance is proxying as well as those of any additional 
 * interfaces it currently provides implementations for.<br>
 * 
 * Whilst this class MAY be used directly, by utililising reflection to call its various methods,
 * it is important to note that: THE GENERATED DYNAMICPROXY CLASS CAN BE FREELY CAST TO THE 
 * MAIN CLASS ITS PROXYING OR ANY OF THE ADDITIONAL INTERFACE TYPES WHERE SPECIFIED.Thus
 * its recommended that the proxy be cast to one of these types as this will allow for 
 * simpler and more performant usage as methods may then be called against these concrete
 * types in the usual way.

 * @author giles
 */
public interface DynamicProxy {
    
    /** 
        Returns the name of the dynamically generated proxy class,
        will typically be the name of the source class its proxying suffixed
        with "Proxy".
        @return String The name of runtime type of the proxy.
    */
    public String getDynamicProxyClassName();
    
    /** 
        Gets the name of the source class this DynamicProxy instance is 
        proxying.
        @return String The name of the source class this DynamicProxy instance is
                       proxying.
         
     */
    public String getSourceClassName();
    
    /**
     * Returns reference to the source class instance this
     * DynamicProxy instance is currently proxying method
     * calls for.
     * @return Object An instance of the underlying class this 
     *                Dynamic proxy is currently proxying calls
     *                for.
     */
    public Object getSourceClassInstance();
    
    /**
     * Returns reference to the ProxyHandler this DynamicProxy instance
     * is currently delegating calls against its public interface to.
     * @return ProxyHandler The user specified ProxyHandler implementation
     *                      this DynamicProxy instance is currently delegating 
     *                      calls against its public interface to.
     */
    public ProxyHandler getProxyHandler();
    
    /**
     * An optional array of additional interfaces that, when specified,
     * are also implemented by the DynamicProxy instance.All calls to
     * these interface methods are delegated to the specified ProxyHandler
     * instance in the same manner.
     * @return Class[] An array of optional interfaces that, when specified,
     *                 are implemented by the DynamicProxy.
     */
    public Class[] getAdditionalInterfaces();
    
}
