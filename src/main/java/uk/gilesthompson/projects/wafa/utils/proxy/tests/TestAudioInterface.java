/*
 * Copyright 2019 Giles Thompson.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package uk.gilesthompson.projects.wafa.utils.proxy.tests;

/**
 * Used to Test the Dynamic Proxy's handling of additional interfaces.
 * @author giles
 */
public interface TestAudioInterface {
    
    public void play();
    
    public void play(String track);
    
    public void stop();
    
    public double pause(); //returns the current pause time.
    
    public void seek(long position);
    
    
    
}
