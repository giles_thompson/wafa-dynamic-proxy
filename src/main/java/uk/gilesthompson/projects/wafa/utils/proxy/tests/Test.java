/*
 * Copyright 2019 Giles Thompson.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package uk.gilesthompson.projects.wafa.utils.proxy.tests;




import java.io.IOException;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import uk.gilesthompson.projects.wafa.utils.proxy.DynamicProxy;
import uk.gilesthompson.projects.wafa.utils.proxy.DynamicProxyFactory;
import uk.gilesthompson.projects.wafa.utils.proxy.exceptions.ProxyGenerationException;


/**
 *
 * @author giles
 */
public class Test {
    
    
    public static void main(String[] args) throws IOException{
        
       
        try {
            new Test().generateAndLoadProxy();
        } 
     

        catch (ProxyGenerationException ex) {
            Logger.getLogger(Test.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    public void generateAndLoadProxy() throws ProxyGenerationException{
        
         /**
            Generate new DynamicProxy via the DynamicProxyFactory the Factory is
            hread safe and thus the same instance can be used to create multiple
            DynamicProxy instances,concurrently if necessary. 
         */
         DynamicProxyFactory dynamicProxyFactory = DynamicProxyFactory.create();
         DynamicProxy dynamicProxy = dynamicProxyFactory.newDynamicProxy(new TestModel(),
                                                                         new TestProxyHandler(),
                                                                         TestAudioInterface.class);
         
         
         
        /**
          Before casting to various concrete types, use the DynamicProxy abstract type
          to obtain basic details about the DynamicProxy instance. 
        */
        System.out.println("DynamicProxy instance details are as follows: ");
        System.out.println("");
        System.out.println("Dynamic proxy class name: "+dynamicProxy.getDynamicProxyClassName());
        System.out.println("Source class name: "+dynamicProxy.getSourceClassName());
        System.out.println("Additional interfaces: "+Arrays.toString(dynamicProxy.getAdditionalInterfaces()));
        System.out.println("Proxy Handler class instance: "+dynamicProxy.getProxyHandler());
        System.out.println("Source class instance: "+dynamicProxy.getSourceClassInstance());
         
         
        /**Cast to the CONCRETE TYPE the DynamicProxy was instantiated to proxy,
           in this case the TestModel class. */
        System.out.println("");
        System.out.println("Casting DynamicProxy to concrete type: TestModel. Executing methods...");
        System.out.println("");
        TestModel testModel2 = (TestModel)dynamicProxy;
        testModel2.getFirstName(); //proxy should print method name
        testModel2.getSurname();
        testModel2.setFirstName("giles");
        testModel2.setSurname("Thompson");
        testModel2.setId(3); //need to add support for dealing with null
        testModel2.getId();
        testModel2.setHeight(1.45f);
        testModel2.getHeight();
        testModel2.setAlreadyRegistered(true);
        testModel2.isAlreadyRegistered();
        testModel2.setMaleOrFemale('M');
        testModel2.getMaleOrFemale();
        testModel2.setPercentageOfFans(15.56);
        testModel2.getPercentageOfFans();
        testModel2.setPointsCollected(35635);
        testModel2.getPointsCollected();
        testModel2.setTimesBeen((byte)7);
        testModel2.getTimesBeen();
        testModel2.setTimesFamilyBeen((short)400);
        testModel2.getTimesFamilyBeen();
        
        
        /**
           Next we cast the DynamicProxy to one of our additional abstract interfaces 
           and execute its methods.The developer may specify and cast to as many 
           abstract interface types as necessary.
        */
        System.out.println("");
        System.out.println("Casting DynamicProxy to abstract interface type: TestAudioInterface. Executing methods....");
        System.out.println("");
        TestAudioInterface taInterface = (TestAudioInterface)dynamicProxy;
        taInterface.play();
        taInterface.play("fav");
        taInterface.pause();
        //taInterface.seek(575857L);
        taInterface.stop();
        
        
        //Test AbstractProxyHandler derived ProxyHandler
        System.out.println("");
        System.out.println("AbstractProxyHandler default forwarding tests:");
        System.out.println("");
        DynamicProxyFactory dynamicProxyFactory2 = DynamicProxyFactory.create();
        DynamicProxy dynamicProxy2 = dynamicProxyFactory2.newDynamicProxy(new User(),
                                                                          new AnotherTestProxyHandler());
        
       
        
         User tm = (User)dynamicProxy2;
         
         tm.setId(1);
         tm.setFirstName("Giles");
         tm.setSurname("Thompson");
         System.out.println("getId value: "+tm.getId());
         System.out.println("getFirstName value: "+tm.getFirstName());
         System.out.println("getSurname value: "+tm.getSurname());
         
         
         
             
         
       

    }
    
    
    static class MyClassLoader extends ClassLoader{

        public static MyClassLoader newInst(ClassLoader parent){
            
            return new MyClassLoader(parent);
        }
        
        MyClassLoader(ClassLoader parent){
            
            super(parent);
        }
      
        public Class loadClassBytes(byte[] classBytes){
            
            Class loadedClass = super.defineClass(null,classBytes, 0, classBytes.length);
            super.resolveClass(loadedClass);
            return loadedClass;
        }

        @Override
        public Class<?> loadClass(String name) throws ClassNotFoundException {
            return super.loadClass(name); //To change body of generated methods, choose Tools | Templates.
        }

        
        /**
        @Override
        public Class<?> loadClass(String name) throws ClassNotFoundException {
            
            InputStream is = null;
            ByteArrayOutputStream baos = null;
            try {
                String classInternalName = "/"+name.replace(".","/").concat(".class");
                System.out.println("Class to be loaded internal name: "+classInternalName);
                is = this.getClass()
                         .getClassLoader()
                         .getResourceAsStream(classInternalName);
                
                baos = new ByteArrayOutputStream();
                int bytesRead;
                byte[] buffer = new byte[1024]; //1k buffer
                while((bytesRead = is.read(buffer,0, buffer.length)) > -1)
                    baos.write(buffer,0,bytesRead);
                    
                    
                return this.loadClassBytes(baos.toByteArray());
            } 
            catch (IOException ex) {
                Logger.getLogger(Test.class.getName()).log(Level.SEVERE, null, ex);
                return null;
            }
            finally{
                
                if(is != null)
                    try{is.close();}catch(IOException ex){}
                
                if(baos != null)
                    try{baos.close();}catch(IOException ex){}
            }
        }
        */
        
        
      
    }
}
