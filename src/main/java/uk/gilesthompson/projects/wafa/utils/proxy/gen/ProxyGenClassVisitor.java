/*
 * Copyright 2019 Giles Thompson.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package uk.gilesthompson.projects.wafa.utils.proxy.gen;


import java.lang.reflect.Method;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.FieldVisitor;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.Type;
import uk.gilesthompson.projects.wafa.utils.proxy.DynamicProxy;

/**
 *
 * @author giles
 */
final class ProxyGenClassVisitor extends ClassVisitor implements Opcodes{
    
    private String proxyClassName;
    private String superClassName;
    private final ClassVisitor clsv;
    private Class[] additionalInterfaces;
    private boolean constructorGenerated;
    ProxyGenClassVisitor(final ClassVisitor cvisit) {
        
        super(Opcodes.ASM6, cvisit);
        this.clsv = cvisit;
    }
    
    ProxyGenClassVisitor(final ClassVisitor cvisit,final Class[] additionalInterfaces) {
        
      this(cvisit);
      this.additionalInterfaces = additionalInterfaces;
    }
    
    
    
    @Override
    public void visit(int version, int access, String name, String signature, String superName, String[] interfaces) {
        
       /** We set our class name to the  and our super class
           to the name of the class we are proxying.*/
       this.proxyClassName = name.concat("Proxy");
       this.superClassName = name;
       
       //prepare all interfaces the DynamicProxy is required to implement
       String[] allInterfaces = this.prepareAllInterfacesArray(interfaces);
       
       clsv.visit(version, access,this.proxyClassName, signature,name,allInterfaces); 
        
    }
    
  

    @Override
    public FieldVisitor visitField(int access, String name, String descriptor, String signature, Object value) {
        //we want to exclude all the fields in the original class.
        return null;
    }

    @Override
    public MethodVisitor visitMethod(int access, String name, String descriptor, String signature, String[] exceptions) {
        
        
        if(name.equals("<init>") && access == ACC_PUBLIC && !this.constructorGenerated){
           
           this.constructorGenerated = true;
           return this.generateDynamicProxyConstructor(access, name, descriptor, signature, exceptions);
        
        }
        else if(access == ACC_PUBLIC)
            return this.generateDynamicProxyPublicInterfaceProxyingMethod(ACC_PUBLIC, name, descriptor, signature, exceptions);
        else
            return null;
 
    }
    
    
    
    
    /**Here we will generate all requisite additional fields: 
       1)A variable to hold an instance of the original class that is required to be proxied
       2)The developer defined ProxyHandler subclass
       3)An array of type Class to hold any optional additional interfaces that are required to 
         be implemented by the generated proxy.
     */
    @Override
    public void visitEnd() {
        
        this.generateDynamicProxyFields();
        
        this.generateDynamicProxyAdditionalInterfacesMethodImpelmentations();
        
        this.generateDynamicProxyInterfaceMethods();
        
        super.visitEnd(); 
    }
    

   
    
    
    
    
                                    //PRIVATE HELPER METHODS.
    
    private void generateDynamicProxyInterfaceMethods() {
        
        MethodVisitor getProxyNameMethod = this.clsv.visitMethod(ACC_PUBLIC,"getDynamicProxyClassName","()Ljava/lang/String;",null,null);
        getProxyNameMethod.visitCode();
        getProxyNameMethod.visitLdcInsn(this.proxyClassName);
        getProxyNameMethod.visitInsn(ARETURN);
        getProxyNameMethod.visitMaxs(0, 0);
        getProxyNameMethod.visitEnd();
        
        MethodVisitor getSourceNameMethod = this.clsv.visitMethod(ACC_PUBLIC,"getSourceClassName","()Ljava/lang/String;",null,null);
        getSourceNameMethod.visitCode();
        getSourceNameMethod.visitLdcInsn(this.superClassName);
        getSourceNameMethod.visitInsn(ARETURN);
        getSourceNameMethod.visitMaxs(0, 0);
        getSourceNameMethod.visitEnd();
        
        MethodVisitor getSourceClassMethod = this.clsv.visitMethod(ACC_PUBLIC,"getSourceClassInstance","()Ljava/lang/Object;",null,null);
        getSourceClassMethod.visitCode();
        getSourceClassMethod.visitVarInsn(ALOAD, 0);
        getSourceClassMethod.visitFieldInsn(GETFIELD, proxyClassName,"proxiedObj","Ljava/lang/Object;");
        getSourceClassMethod.visitInsn(ARETURN);
        getSourceClassMethod.visitMaxs(0, 0);
        getSourceClassMethod.visitEnd();
        
        MethodVisitor getProxyHandlerMethod = this.clsv.visitMethod(ACC_PUBLIC,"getProxyHandler","()Luk/gilesthompson/projects/wafa/utils/proxy/utilities/ProxyHandler;",null,null);
        getProxyHandlerMethod.visitCode();
        getProxyHandlerMethod.visitVarInsn(ALOAD, 0);
        getProxyHandlerMethod.visitFieldInsn(GETFIELD, proxyClassName,"proxyHandler","Luk/gilesthompson/projects/wafa/utils/proxy/utilities/ProxyHandler;");
        getProxyHandlerMethod.visitInsn(ARETURN);
        getProxyHandlerMethod.visitMaxs(0, 0);
        getProxyHandlerMethod.visitEnd();
        
        MethodVisitor getAdditionalInterfacesMethod = this.clsv.visitMethod(ACC_PUBLIC,"getAdditionalInterfaces","()[Ljava/lang/Class;",null,null);
        getAdditionalInterfacesMethod.visitCode();
        getAdditionalInterfacesMethod.visitVarInsn(ALOAD, 0);
        getAdditionalInterfacesMethod.visitFieldInsn(GETFIELD, proxyClassName,"additionalInterfaces","[Ljava/lang/Class;");
        getAdditionalInterfacesMethod.visitInsn(ARETURN);
        getAdditionalInterfacesMethod.visitMaxs(0, 0);
        getAdditionalInterfacesMethod.visitEnd();
    }
    
    private String[] prepareAllInterfacesArray(String[] interfaces){
        
       String[] allInterfaces;
       if(interfaces == null || interfaces.length < 1){
           
           if(this.additionalInterfaces != null){
           
               allInterfaces = new String[this.additionalInterfaces.length+1];
               for(int i = 0; i < this.additionalInterfaces.length;i++)
                   allInterfaces[i] = this.additionalInterfaces[i].getName()
                                                                  .replace(".","/");
               
               allInterfaces[this.additionalInterfaces.length] = DynamicProxy.class
                                                                             .getName()
                                                                             .replace(".","/");
           }
           else
               allInterfaces = new String[]{DynamicProxy.class.getName().replace(".","/")};
       }
       else{
           if(this.additionalInterfaces != null){
           
               allInterfaces = new String[(interfaces.length+this.additionalInterfaces.length+1)];
           
              //first add interfaces declared on the main class.
              for(int i = 0; i < interfaces.length;i++)
               allInterfaces[i] = interfaces[i];
           
             //then add additional interfaces
             int additionalInterfaceStartIndex = interfaces.length -1;
             for(int i = additionalInterfaceStartIndex; i < this.additionalInterfaces.length; i++){
               
               allInterfaces[i] = this.additionalInterfaces[i].getName()
                                                              .replace(".", "/");
             }
             
             //then finally we add DynamicProxy interface
             allInterfaces[(interfaces.length+this.additionalInterfaces.length)] = DynamicProxy.class
                                                                                               .getName()
                                                                                               .replace(".","/");
             
           }
           else{
               allInterfaces = new String[interfaces.length+1];
               
               for(int i = 0; i < interfaces.length; i++)
                   allInterfaces[i] = interfaces[i];
               
               //finally add our DynamicProxy interface
               allInterfaces[interfaces.length] = DynamicProxy.class
                                                              .getName()
                                                              .replace(".", ",");
           }
       }
       
       return allInterfaces;
    }
    
    private void generateDynamicProxyAdditionalInterfacesMethodImpelmentations(){
        
        /**
            Implement all methods declared in the additional interfaces
            where specified.
         */
        if(additionalInterfaces == null)
            return;
        
        for(Class currentInterface : additionalInterfaces){
            
           Method[] curInterfaceMethods =  currentInterface.getMethods();
           
           for(Method curMethod : curInterfaceMethods){
               
               //First generate descriptor.
               Class[] methodParamNativeTypes = curMethod.getParameterTypes();
               Type[] methodParamAsmTypes = new Type[methodParamNativeTypes.length];
               for(int i = 0; i < methodParamNativeTypes.length; i++)
                   methodParamAsmTypes[i] = Type.getType(methodParamNativeTypes[i]);

               String methodDescriptor = Type.getMethodDescriptor(Type.getType(curMethod.getReturnType()),methodParamAsmTypes);
               
               //next the method name
               String methodName = curMethod.getName();
               
               //next exceptions (we pass this in as an empty array it will be populated in te method)
               String[] exceptions = new String[0];
               
               //finally access which will obviously be always be public
               int access = ACC_PUBLIC;
               
               //get method signature where one exists (may be null for non generic methods)
               String methodSignature = this.getMethodSignature(curMethod);
               
               //we now have all the detail necessary to generate the proxy method
               this.generateDynamicProxyPublicInterfaceProxyingMethod(access, methodName, methodDescriptor,methodSignature,exceptions);
               
           }
                  
        }
        
    }
    
    private String getMethodSignature(Method aMethod) {
    
        try {
            java.lang.reflect.Field f = java.lang.reflect.Method.class.getDeclaredField("signature");
            f.setAccessible(true);
            String signature = (String)f.get(aMethod);
            return signature;
        } 
        catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException ex) {
            Logger.getLogger(ProxyGenClassVisitor.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    
    private void generateDynamicProxyFields(){
        
        this.clsv.visitField(ACC_PRIVATE,"proxiedObj","Ljava/lang/Object;",null,null);
        this.clsv.visitField(ACC_PRIVATE,"proxyHandler","Luk/gilesthompson/projects/wafa/utils/proxy/utilities/ProxyHandler;",null,null);
        this.clsv.visitField(ACC_PRIVATE,"additionalInterfaces","[Ljava/lang/Class;",null,null);
    }

    private MethodVisitor generateDynamicProxyConstructor(int access, String name, String descriptor, String signature, String[] exceptions){
        
            MethodVisitor mv =  this.clsv.visitMethod(access, name,"(Ljava/lang/Object;Luk/gilesthompson/projects/wafa/utils/proxy/utilities/ProxyHandler;[Ljava/lang/Class;)V",signature,exceptions);
            mv.visitCode();
            mv.visitVarInsn(ALOAD,0); //load THIS pointer
            mv.visitMethodInsn(INVOKESPECIAL,this.superClassName,"<init>","()V",false);
            
            //initialise fields
             mv.visitVarInsn(ALOAD, 0); //THIS pointer
             mv.visitVarInsn(ALOAD, 1); //first param (our object to proxy)
             mv.visitFieldInsn(PUTFIELD,this.proxyClassName, "proxiedObj","Ljava/lang/Object;");
            
            mv.visitVarInsn(ALOAD, 0); //THIS pointer
            mv.visitVarInsn(ALOAD, 2); //second param (our developer-defined ProxyHandler instance.)
            mv.visitFieldInsn(PUTFIELD,this.proxyClassName, "proxyHandler","Luk/gilesthompson/projects/wafa/utils/proxy/utilities/ProxyHandler;");
            
            mv.visitVarInsn(ALOAD, 0); //THIS pointer
            mv.visitVarInsn(ALOAD, 3); //third and final param (an optional array of additional interfaces that the proxy should implement.)
            mv.visitFieldInsn(PUTFIELD,this.proxyClassName, "additionalInterfaces","[Ljava/lang/Class;");
            
            
            mv.visitInsn(RETURN);
            mv.visitMaxs(0,0);
            mv.visitEnd();          
            return mv;
        
    }
    
    /** 
        Generates a ParamValue array for the current method and places it at the top
        of the current thread stack. 
     */
    private MethodVisitor generateParamValueArray(MethodVisitor mv,Type[] methodTypes){
        
        mv.visitIntInsn(BIPUSH, methodTypes.length);
        mv.visitTypeInsn(ANEWARRAY,"uk/gilesthompson/projects/wafa/utils/proxy/utilities/ParamValue");
        
        //temporerily store the array to a local varriable location we can be assured wont be occcupied
        //here we pick a method types length + 10.
        int arrayTempStoreIdx = methodTypes.length+10;
        mv.visitVarInsn(ASTORE,arrayTempStoreIdx);
        
        //next for each of our method types we:
        //1)load the array to the stack. 
        //2)specify an array storage index as a constant value.
        //3)load the paramter value to the stack (the OPCODE we use will depend on the value)
        //4)Call the appropriate ParamValue Factory method for the parameter value this 
        //  takes the parameter value an arguemnt and returns a ParamValue instance to the
        //  top of the stack
        //5)Finnaly we execute the object array storage 
        
        //once our array is populated we reload it to the top of the stack before
        //yeilding to the method that called us.
        for(int i = 0; i < methodTypes.length; i++){
            
            //param index starts from 0 as THIS pointer is at index 0
            int curLocalVarIdx = i+1;
            
            mv.visitVarInsn(ALOAD,arrayTempStoreIdx);
            mv.visitLdcInsn(i);
            if(methodTypes[i] == Type.BOOLEAN_TYPE){
                mv.visitVarInsn(ILOAD,curLocalVarIdx);
                mv.visitMethodInsn(INVOKESTATIC,"uk/gilesthompson/projects/wafa/utils/proxy/utilities/ParamValue","newInst","(Z)Luk/gilesthompson/projects/wafa/utils/proxy/utilities/ParamValue;", false); 
            }
            else if(methodTypes[i] == Type.BYTE_TYPE){
                mv.visitVarInsn(ILOAD,curLocalVarIdx);
                mv.visitMethodInsn(INVOKESTATIC,"uk/gilesthompson/projects/wafa/utils/proxy/utilities/ParamValue","newInst","(B)Luk/gilesthompson/projects/wafa/utils/proxy/utilities/ParamValue;", false); 
            }
            else if(methodTypes[i] == Type.CHAR_TYPE){
                mv.visitVarInsn(ILOAD,curLocalVarIdx);
                mv.visitMethodInsn(INVOKESTATIC,"uk/gilesthompson/projects/wafa/utils/proxy/utilities/ParamValue","newInst","(C)Luk/gilesthompson/projects/wafa/utils/proxy/utilities/ParamValue;", false); 
            }
            else if(methodTypes[i] == Type.SHORT_TYPE){
                mv.visitVarInsn(ILOAD,curLocalVarIdx);
                mv.visitMethodInsn(INVOKESTATIC,"uk/gilesthompson/projects/wafa/utils/proxy/utilities/ParamValue","newInst","(S)Luk/gilesthompson/projects/wafa/utils/proxy/utilities/ParamValue;", false); 
            }
            else if(methodTypes[i] == Type.INT_TYPE){
                mv.visitVarInsn(ILOAD,curLocalVarIdx);
                mv.visitMethodInsn(INVOKESTATIC,"uk/gilesthompson/projects/wafa/utils/proxy/utilities/ParamValue","newInst","(I)Luk/gilesthompson/projects/wafa/utils/proxy/utilities/ParamValue;", false); 
            }
            else if(methodTypes[i] == Type.FLOAT_TYPE){
                mv.visitVarInsn(FLOAD,curLocalVarIdx);
                mv.visitMethodInsn(INVOKESTATIC,"uk/gilesthompson/projects/wafa/utils/proxy/utilities/ParamValue","newInst","(F)Luk/gilesthompson/projects/wafa/utils/proxy/utilities/ParamValue;", false); 
            }
            else if(methodTypes[i] == Type.DOUBLE_TYPE){
                mv.visitVarInsn(DLOAD,curLocalVarIdx);
                mv.visitMethodInsn(INVOKESTATIC,"uk/gilesthompson/projects/wafa/utils/proxy/utilities/ParamValue","newInst","(D)Luk/gilesthompson/projects/wafa/utils/proxy/utilities/ParamValue;", false); 
            }
            else if(methodTypes[i] == Type.LONG_TYPE){
                mv.visitVarInsn(LLOAD,curLocalVarIdx);
                mv.visitMethodInsn(INVOKESTATIC,"uk/gilesthompson/projects/wafa/utils/proxy/utilities/ParamValue","newInst","(J)Luk/gilesthompson/projects/wafa/utils/proxy/utilities/ParamValue;", false); 
               
            }
            else{
                mv.visitVarInsn(ALOAD,curLocalVarIdx);
                mv.visitMethodInsn(INVOKESTATIC,"uk/gilesthompson/projects/wafa/utils/proxy/utilities/ParamValue","newInst","(Ljava/lang/Object;)Luk/gilesthompson/projects/wafa/utils/proxy/utilities/ParamValue;", false); 
            }
                
            
            
            mv.visitInsn(AASTORE);
        }
        
        //finally reload the array to the top of the stack
        mv.visitVarInsn(ALOAD,arrayTempStoreIdx);
        
        
        
        
        return mv;
    }
    
    private MethodVisitor generateDynamicProxyPublicInterfaceProxyingMethod(int access, String name, String descriptor, String signature, String[] exceptions){
        //System.out.println("Visit method: "+name+" called");
        /**We only change the method defintion of public methods 
           1)Firstly we need to generate byte code that obtain access to
              the specific method via reflection.We store the method to a local
              for later use.<br>
            
           2)Next we load the user-defined proxyhandler implementation to the stack
           * the proxyhandler class is supplied to the proxys constructor
             we will now begin preparing the parameters to call its "handle" method
             * 
           3)First we need to load the original class instance (supplied to the 
             proxy constructor and stored as a local) to the stack
             * 
           4)Next we load the Method instance back to the stack from the local we
             stored it to in step one
             * 
           5)At this point we are ready to issue a call to the ProxyHandler's
           * "handleMethodInvocation method which will replace an Object return
           * value at the top of the stack
           * 
           6)finally we generate byte code to cast that object to the appropriate
           * type based on the current methods return type signature.<br>
           * NOTE: VOID types are a special case in that we will first need
           *       to POP the Object value returned from the call to the ProxyHandler's 
           *       method of the stack before generating a RETURN instruction as suppose to
           *       a ARETURN instruction.
           * 
           */
        
        //we add NoSuchMethodException and SecurityException to the exceptions
            //declared by the original class.
            
            String[] allExceptions;
            int currentExceptionCount = 0;
            if(exceptions != null){
                currentExceptionCount = exceptions.length;
                allExceptions = new String[(currentExceptionCount+2)];
                for(int i = 0; i < currentExceptionCount; i++)
                    allExceptions[i] = exceptions[i];
            }
            else
                allExceptions = new String[2];
            
            allExceptions[currentExceptionCount] = "java/lang/NoSuchMethodException";
            allExceptions[currentExceptionCount+1] = "java/lang/SecurityException";
                
           
            MethodVisitor mv =  this.clsv.visitMethod(access, name, descriptor, signature,allExceptions);
            mv.visitCode();
            mv.visitVarInsn(ALOAD, 0); //load THIS pointer
            mv.visitMethodInsn(INVOKEVIRTUAL,this.proxyClassName,"getClass","()Ljava/lang/Class;",false); //get class.
            mv.visitLdcInsn(name); //load the name of this method to the stack.
            
      
            
            //convert the method types as defined in the signature to a type array
            Type[] methodTypes = Type.getArgumentTypes(descriptor);
            
            //push the size of the array to the stack which should be equal to the number of parameters.
            mv.visitIntInsn(BIPUSH, methodTypes.length);
            mv.visitTypeInsn(ANEWARRAY,"java/lang/Class");
            
            //store the array to the next local variable index
            int arrayStoreIdx = (methodTypes.length+2);
            
            mv.visitVarInsn(ASTORE,arrayStoreIdx);
            
            //iterate over our types, for each one we  load our array 
            //to the stack and store the type at the current index.
            for(int i = 0; i < methodTypes.length; i++){
                
                //load array object to the stack
                mv.visitVarInsn(ALOAD, arrayStoreIdx);
                
                //store the type to the current index
                mv.visitLdcInsn(i);
                if(methodTypes[i] == Type.INT_TYPE)
                    mv.visitFieldInsn(GETSTATIC, "java/lang/Integer", "TYPE", "Ljava/lang/Class;");
                else if(methodTypes[i] == Type.FLOAT_TYPE)
                    mv.visitFieldInsn(GETSTATIC, "java/lang/Float", "TYPE", "Ljava/lang/Class;");
                else if(methodTypes[i] == Type.DOUBLE_TYPE)
                    mv.visitFieldInsn(GETSTATIC, "java/lang/Double", "TYPE", "Ljava/lang/Class;");
                else if(methodTypes[i] == Type.LONG_TYPE)
                    mv.visitFieldInsn(GETSTATIC, "java/lang/Long", "TYPE", "Ljava/lang/Class;");
                else if(methodTypes[i] == Type.SHORT_TYPE)
                    mv.visitFieldInsn(GETSTATIC, "java/lang/Short", "TYPE", "Ljava/lang/Class;");
                else if(methodTypes[i] == Type.BYTE_TYPE)
                    mv.visitFieldInsn(GETSTATIC, "java/lang/Byte", "TYPE", "Ljava/lang/Class;");
                else if(methodTypes[i] == Type.CHAR_TYPE)
                    mv.visitFieldInsn(GETSTATIC, "java/lang/Character", "TYPE", "Ljava/lang/Class;");
                else if(methodTypes[i] == Type.BOOLEAN_TYPE)
                    mv.visitFieldInsn(GETSTATIC, "java/lang/Boolean", "TYPE", "Ljava/lang/Class;");
                else
                    mv.visitLdcInsn(methodTypes[i]);  
                
                mv.visitInsn(AASTORE);
                

            }
            
            
           
            //load our now populaed array back to the stack
            mv.visitVarInsn(ALOAD,arrayStoreIdx);
            
            //we now make a call to the class "getMethod"
            mv.visitMethodInsn(INVOKEVIRTUAL,"java/lang/Class","getMethod","(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;",false);

            //store method to a local
            mv.visitVarInsn(ASTORE,arrayStoreIdx+1);
            
            //load our ProxyHandler to the stack
             mv.visitVarInsn(ALOAD, 0); //first this pointer
             mv.visitFieldInsn(GETFIELD,this.proxyClassName,"proxyHandler","Luk/gilesthompson/projects/wafa/utils/proxy/utilities/ProxyHandler;");
            
            //next load our proxied object to the stack
            mv.visitVarInsn(ALOAD, 0); //first this pointer
            mv.visitFieldInsn(GETFIELD,this.proxyClassName,"proxiedObj","Ljava/lang/Object;"); //then field
            
            //reload our method back to the stack
            mv.visitVarInsn(ALOAD,arrayStoreIdx+1);
            
            //finally call into our helper method to generate a ParamValue array
            //the method will place the resulting array at the top of the stack
            this.generateParamValueArray(mv,methodTypes);
            
            //then we invoke ProxyHandler interface (implementation) handling method
            mv.visitMethodInsn(INVOKEINTERFACE,"uk/gilesthompson/projects/wafa/utils/proxy/utilities/ProxyHandler","handleMethodInvocation","(Ljava/lang/Object;Ljava/lang/reflect/Method;[Luk/gilesthompson/projects/wafa/utils/proxy/utilities/ParamValue;)Ljava/lang/Object;", true);

           //what we do here will be dependent on the declared return type of the method
          
           if(Type.getReturnType(descriptor) == Type.VOID_TYPE){
               
                 mv.visitInsn(POP);
                 mv.visitInsn(RETURN);
            }
           else if(!this.typeIsPrimitive(Type.getReturnType(descriptor))
                   && Type.getReturnType(descriptor).getInternalName().length() > 1){
           
               //mv.visitInsn(ACONST_NULL);
               mv.visitTypeInsn(CHECKCAST,Type.getReturnType(descriptor).getInternalName());
               mv.visitInsn(ARETURN);
               
           }
          
           else if(Type.getReturnType(descriptor) == Type.INT_TYPE){
               
               mv.visitTypeInsn(CHECKCAST,"java/lang/Integer");
               mv.visitMethodInsn(INVOKESTATIC,"uk/gilesthompson/projects/wafa/utils/proxy/utilities/PUtil","zeroIfNull","(Ljava/lang/Integer;)Ljava/lang/Integer;",false);
               mv.visitMethodInsn(INVOKEVIRTUAL,"java/lang/Integer","intValue","()I",false);
               mv.visitInsn(IRETURN);
     
           }
           
           else if(Type.getReturnType(descriptor) == Type.FLOAT_TYPE){
               mv.visitTypeInsn(CHECKCAST,"java/lang/Float");
               mv.visitMethodInsn(INVOKESTATIC,"uk/gilesthompson/projects/wafa/utils/proxy/utilities/PUtil","zeroIfNull","(Ljava/lang/Float;)Ljava/lang/Float;",false);
               mv.visitMethodInsn(INVOKEVIRTUAL,"java/lang/Float","floatValue","()F",false);
               mv.visitInsn(FRETURN);
           }
           
           else if(Type.getReturnType(descriptor) == Type.DOUBLE_TYPE){
               mv.visitTypeInsn(CHECKCAST,"java/lang/Double");
               mv.visitMethodInsn(INVOKESTATIC,"uk/gilesthompson/projects/wafa/utils/proxy/utilities/PUtil","zeroIfNull","(Ljava/lang/Double;)Ljava/lang/Double;",false);
               mv.visitMethodInsn(INVOKEVIRTUAL,"java/lang/Double","doubleValue","()D",false);
               mv.visitInsn(DRETURN);
           }
           
           
           else if(Type.getReturnType(descriptor) == Type.LONG_TYPE){
               mv.visitTypeInsn(CHECKCAST,"java/lang/Long");
               mv.visitMethodInsn(INVOKESTATIC,"uk/gilesthompson/projects/wafa/utils/proxy/utilities/PUtil","zeroIfNull","(Ljava/lang/Long;)Ljava/lang/Long;",false);
               mv.visitMethodInsn(INVOKEVIRTUAL,"java/lang/Long","longValue","()J",false);
               mv.visitInsn(LRETURN);
   
           }
          
           else if(Type.getReturnType(descriptor) == Type.BOOLEAN_TYPE){
               mv.visitTypeInsn(CHECKCAST,"java/lang/Boolean");
               mv.visitMethodInsn(INVOKESTATIC,"uk/gilesthompson/projects/wafa/utils/proxy/utilities/PUtil","zeroIfNull","(Ljava/lang/Boolean;)Ljava/lang/Boolean;",false);
               mv.visitMethodInsn(INVOKEVIRTUAL,"java/lang/Boolean","booleanValue","()Z",false);
               mv.visitInsn(IRETURN);
           }
            else if(Type.getReturnType(descriptor) == Type.CHAR_TYPE){
               mv.visitTypeInsn(CHECKCAST,"java/lang/Character");
               mv.visitMethodInsn(INVOKESTATIC,"uk/gilesthompson/projects/wafa/utils/proxy/utilities/PUtil","zeroIfNull","(Ljava/lang/Character;)Ljava/lang/Character;",false);
               mv.visitMethodInsn(INVOKEVIRTUAL,"java/lang/Character","charValue","()C",false);
               mv.visitInsn(IRETURN);
               
               
           }
           
           else if(Type.getReturnType(descriptor) == Type.SHORT_TYPE){
               mv.visitTypeInsn(CHECKCAST,"java/lang/Short");
               mv.visitMethodInsn(INVOKESTATIC,"uk/gilesthompson/projects/wafa/utils/proxy/utilities/PUtil","zeroIfNull","(Ljava/lang/Short;)Ljava/lang/Short;",false);
               mv.visitMethodInsn(INVOKEVIRTUAL,"java/lang/Short","shortValue","()S",false);
               mv.visitInsn(IRETURN);
               
               
           }
            else if(Type.getReturnType(descriptor) == Type.BYTE_TYPE){
               mv.visitTypeInsn(CHECKCAST,"java/lang/Byte");
               mv.visitMethodInsn(INVOKESTATIC,"uk/gilesthompson/projects/wafa/utils/proxy/utilities/PUtil","zeroIfNull","(Ljava/lang/Byte;)Ljava/lang/Byte;",false);
               mv.visitMethodInsn(INVOKEVIRTUAL,"java/lang/Byte","byteValue","()B",false);
               mv.visitInsn(IRETURN);
               
               
           }
                  
            //mv.visitInsn(ARETURN);
           mv.visitMaxs(0, 0);
           mv.visitEnd();
            
           return mv;
    }

    
    public boolean typeIsPrimitive(Type aType){
        
        return(aType == Type.BOOLEAN_TYPE
           || aType == Type.BYTE_TYPE
           || aType == Type.CHAR_TYPE
           || aType == Type.DOUBLE_TYPE
           || aType == Type.FLOAT_TYPE
           || aType == Type.INT_TYPE
           || aType == Type.LONG_TYPE
           || aType == Type.SHORT_TYPE);
        
       
    }
    
}
