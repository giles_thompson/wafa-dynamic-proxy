/*
 * Copyright 2019 Giles Thompson.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package uk.gilesthompson.projects.wafa.utils.proxy.tests;

import java.lang.reflect.Method;
import uk.gilesthompson.projects.wafa.utils.proxy.utilities.ParamValue;
import uk.gilesthompson.projects.wafa.utils.proxy.utilities.ProxyHandler;

/**
 *
 * @author giles
 */
public class TestProxyHandler implements ProxyHandler {

    @Override
    public Object handleMethodInvocation(Object proxiedObj, Method method,ParamValue[] paramValues) {
       
        System.out.println("TestProxyHandler::: Request to execute method: "+method.getName());
        System.out.println("Supplied param values were: ");
        for(ParamValue pv : paramValues)
            if(pv.getValueType() == ParamValue.VALUE_TYPE.OBJECT)
                System.out.println(pv.getOvalue());
            else if(pv.getValueType() == ParamValue.VALUE_TYPE.FLOAT){
                System.out.println(pv.getFvalue());
            }
            else if(pv.getValueType() == ParamValue.VALUE_TYPE.BOOLEAN){
                System.out.println(pv.isZvalue());
            }
            else if(pv.getValueType() == ParamValue.VALUE_TYPE.CHAR){
                System.out.println(pv.getCvalue());
            }
            else if(pv.getValueType() == ParamValue.VALUE_TYPE.DOUBLE){
                System.out.println(pv.getDvalue());
            }
        return null;
    }
    
}
