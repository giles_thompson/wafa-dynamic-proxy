/*
 * Copyright 2019 Giles Thompson.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package uk.gilesthompson.projects.wafa.utils.proxy.utilities;

/**
 * Represents the single value of a Parameter passed in to a DynamicProxy
 * method.There will be an instance of this class for each parameter
 * defined in a methods signature.
 * @author giles
 */
public class ParamValue {
    
    private int ivalue;
    private byte bvalue;
    private short svalue;
    private char cvalue;
    private long lvalue;
    private double dvalue;
    private boolean zvalue;
    private float fvalue;
    private Object ovalue;
    private int valueType;

    public ParamValue(int ivalue) {
        this.ivalue = ivalue;
        this.valueType = VALUE_TYPE.INT;
    }

    public ParamValue(byte bvalue) {
        this.bvalue = bvalue;
        this.valueType = VALUE_TYPE.BYTE;
    }

    public ParamValue(short svalue) {
        this.svalue = svalue;
        this.valueType = VALUE_TYPE.SHORT;
    }

    public ParamValue(char cvalue) {
        this.cvalue = cvalue;
        this.valueType = VALUE_TYPE.CHAR;
    }

    public ParamValue(long lvalue) {
        this.lvalue = lvalue;
        this.valueType = VALUE_TYPE.LONG;
    }

    public ParamValue(double dvalue) {
        this.dvalue = dvalue;
        this.valueType = VALUE_TYPE.DOUBLE;
    }

    public ParamValue(boolean zvalue) {
        this.zvalue = zvalue;
        this.valueType = VALUE_TYPE.BOOLEAN;
    }

    public ParamValue(float fvalue) {
        this.fvalue = fvalue;
        this.valueType = VALUE_TYPE.FLOAT;
    }

    public ParamValue(Object ovalue) {
        this.ovalue = ovalue;
        this.valueType = VALUE_TYPE.OBJECT;
    }
    
    
    
    
    public static ParamValue newInst(int value){
        
        return new ParamValue(value);
    }
    
    public static ParamValue newInst(float value){
        
        return new ParamValue(value);
    }
    
     public static ParamValue newInst(byte value){
        
        return new ParamValue(value);
    }
    public static ParamValue newInst(char value){
        
        return new ParamValue(value);
    }
    public static ParamValue newInst(short value){
        
        return new ParamValue(value);
    }
    public static ParamValue newInst(long value){
        
        return new ParamValue(value);
    }
    public static ParamValue newInst(boolean value){
       
        return new ParamValue(value);
    }
    public static ParamValue newInst(double value){
       
        return new ParamValue(value);
    }
    public static ParamValue newInst(Object value){
       
        return new ParamValue(value);
    }
    
    
    public static class VALUE_TYPE{
        
        public static final int INT = 0;
        public static final int BYTE = 1;
        public static final int SHORT = 2;
        public static final int CHAR = 3;
        public static final int LONG = 4;
        public static final int DOUBLE = 5;
        public static final int BOOLEAN = 6;
        public static final int FLOAT = 7;
        public static final int OBJECT = 8;
    }

    public int getValueType() {
        return valueType;
    }

    public int getIvalue() {
        return ivalue;
    }

    public byte getBvalue() {
        return bvalue;
    }

    public short getSvalue() {
        return svalue;
    }

    public char getCvalue() {
        return cvalue;
    }

    public long getLvalue() {
        return lvalue;
    }

    public double getDvalue() {
        return dvalue;
    }

    public boolean isZvalue() {
        return zvalue;
    }

    public float getFvalue() {
        return fvalue;
    }

    public Object getOvalue() {
        return ovalue;
    }
    
    
}
