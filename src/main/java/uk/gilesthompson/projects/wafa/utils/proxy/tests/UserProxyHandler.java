/*
 * Copyright 2019 Giles Thompson.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package uk.gilesthompson.projects.wafa.utils.proxy.tests;

import java.lang.reflect.Method;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import uk.gilesthompson.projects.wafa.utils.proxy.tests.UserLoginStats.UserSession;
import uk.gilesthompson.projects.wafa.utils.proxy.utilities.AbstractProxyHandler;
import uk.gilesthompson.projects.wafa.utils.proxy.utilities.ParamValue;

/**
 * This is the ProxyHandler that all calls to our generated "User DynamicProxy"
 * will delegate to.Here we implement the logic for the additional login
 * statistic methods we defined in the "UserLoginStats" interface; all other
 * method calls are passed-through to the underlying User class unchanged. 
 * @author giles
 */
public class UserProxyHandler extends AbstractProxyHandler {
    
    //Extra map field to store login centric information. Here we store
    //instances of UserSession to the map under the login id.
    private final Map<String,UserSession> userSessions;

    public UserProxyHandler() {
        
        this.userSessions = new HashMap<>();
    }
    
    @Override
    public Object handleMethodInvocation(Object proxiedObj, Method method, ParamValue[] parameterValues) {
        
        /**
           if the method exists in the original concrete "User" class then its NOT related to the login 
           related methods defined in our UserLoginStats interface thus we simply pass the call through,
           otherwise the method MUST be explictly handled here.The "methodExistsInConcreteClass" method 
           provided by our super class will determine this for us.
         */
        if(methodExistsInConcreteClass(proxiedObj, method))
            return super.handleMethodInvocation(proxiedObj, method, parameterValues); 
        else{
            
            switch (method.getName()) {
                case "recordLogin":
                    Date date = (Date)parameterValues[0].getOvalue(); //get the first parameter (i,e the date)
                    String sessId = (String)parameterValues[1].getOvalue(); //get the second param (our session id)
                    UserSessionImpl us = new UserSessionImpl(); //create a UserSession instance and store it to our map.
                    us.setLoginTime(date);
                    synchronized(this.userSessions){this.userSessions.put(sessId, us);}
                    break;
                case "recordLogout":
                    Date  logoutDate = (Date)parameterValues[0].getOvalue(); //get the first parameter (i,e the date)
                    String sId = (String)parameterValues[1].getOvalue(); //get the second param (our session id)
                    synchronized(this.userSessions){this.userSessions.get(sId) //lookup Session and set logout time.
                                                                     .setLogoutTime(logoutDate);}
                   
                    break;
                case "listAllUserSessions":
                    synchronized(this.userSessions){return this.userSessions.values();}
                default:
                    break;
            }
            
            return null;
        }
    }
    
    /**
        Quick implementation of the UserSession Sub Interface defined in the
        UserLoginStats interface. 
     */
     public class UserSessionImpl implements UserLoginStats.UserSession{
        private Date loginTime;
        private Date logoutTime;
        private long sessionDuration;

        @Override
        public Date getLoginTime() {return this.loginTime;}
        @Override
        public Date getLogoutTime() {return this.logoutTime;}
        @Override
        public long getSessionDuration() {return this.sessionDuration;}
        @Override
        public void setLoginTime(Date aDate) {this.loginTime = aDate;}
        @Override
        public void setLogoutTime(Date aDate) {this.logoutTime  = aDate; 
                                               this.sessionDuration = (this.logoutTime.getTime()-this.loginTime.getTime());}
       
         
     }
}
