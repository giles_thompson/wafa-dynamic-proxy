/*
 * Copyright 2019 Giles Thompson.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package uk.gilesthompson.projects.wafa.utils.proxy.tests;

import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * Defines login statistics related methods that we wish our 
 * User DynamicProxy o implement.
 * @author giles
 */
public interface UserLoginStats {
    public void recordLogin(Date time,String sessionId);
    public void recordLogout(Date time,String sessionId);
    public Collection<UserSession> listAllUserSessions();

    /** 
        Sub Interface to be used by our ProxyHandler
        methods defined here will NOT be implemented by the 
        resulting DynamicProxy like those above. 
     */
    public interface UserSession{
        public Date getLoginTime();
        public Date getLogoutTime();
        public long getSessionDuration();
        public void setLoginTime(Date aDate);
        public void setLogoutTime(Date aDate);
    }
}
