/*
 * Copyright 2019 Giles Thompson.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package uk.gilesthompson.projects.wafa.utils.proxy.tests;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Date;
import uk.gilesthompson.projects.wafa.utils.proxy.DynamicProxy;
import uk.gilesthompson.projects.wafa.utils.proxy.DynamicProxyFactory;
import uk.gilesthompson.projects.wafa.utils.proxy.exceptions.ProxyGenerationException;


/**
 * Using DynamicProxy with additional interfaces to enable the
 * addition of new fields and methods to a class at runtime.
 * @author giles
 */
public class AdvancedExample {
    
    
    
    public static void main(String[] args) throws ProxyGenerationException{
        
        /** 
            Create our factory and use it to obtain a new reference to a new DynamicProxy instance; 
            we take care to pass in the concrete "User" class we wish to proxy, Our "ProxyHandler"
            AND this time a single "additional interface" namely our "UserLoginStats" Interface 
            The resulting DynamicProxy will now implement the public interface of BOTH of these 
            components and pass all calls through to both to our handler
            NOTE: Any amount of "additional interfaces" may be specified to add further methods
            *     to the public interface of the DynamicProxy.
       */
        
        DynamicProxyFactory dynamicProxyFactory = DynamicProxyFactory.create();
        DynamicProxy dynamicProxy = dynamicProxyFactory.newDynamicProxy(new User(), 
                                                                        new UserProxyHandler(), 
                                                                        UserLoginStats.class);
        
        
        
        //1)BEFORE WE BEGIN WORKING WITH OUR PROXY LETS PRINT SOME OF ITS DETAILS TO THE CONSOLE.
        System.out.println("THE DYNAMICPROXY INSTANCE DETAILS ARE AS FOLLOWS: ");
        System.out.println("");
        System.out.println("The DynamicProxy internal class name is: "+dynamicProxy.getDynamicProxyClassName());
        System.out.println("The Source class its proxying for is:    "+dynamicProxy.getSourceClassName());
        System.out.println("Additional interfaces it implements are: "+Arrays.toString(dynamicProxy.getAdditionalInterfaces()));
        
        
        //2)WE MAY CAST THE DYNAMICPROXY TO  A USER TO WORK WITH IT AS A USER CLASS WE DID IN THE BASIC EXAMPLE
        //ALL CALLS ARE PASSED THROUGH TO THE UNDERLYING INSTANCE AND THIS IT IS INDISTINGUISHABLE FOR THE ORIGINAL CLASS
        System.out.println("");
        System.out.println("USER DETAILS ARE AS FOLLOWS: ");
        System.out.println("");
        User user = (User)dynamicProxy;
        user.setId(1);
        user.setFirstName("Giles");
        user.setSurname("Thompson");
        System.out.println("User id value is: "+user.getId());
        System.out.println("User first name value is: "+user.getFirstName());
        System.out.println("User surname value is: "+user.getSurname());
        
        
        //3)OK SO FAR SO GOOD, BUT THE WHOLE POINT OF THIS EXERCISE WAS TO ADD **EXTRA** OPERATIONS
        //  AT RUNTIME. WHICH WE HAVE DONE BY SPECIFYING AN "ADDITIONAL INTERFACE" AND PROVIDING AN
        //  IMPLEMENTATION FOR ITS METHODS IN OUR PROXYHANDLER. ACCESSING THESE EXTA METHODS
        //  COULDNT BE SIMPLER WE JUST CAST THE **SAME** DYNAMICPROXY INSTANCE TO THAT INTERFACE...
        UserLoginStats userLoginStats = (UserLoginStats) dynamicProxy;
        
        //simulate a bunch of logins and logouts...
        userLoginStats.recordLogin(new Date(),"101");
        userLoginStats.recordLogout(new Date(System.currentTimeMillis()+1000*60*30),"101");
        
        userLoginStats.recordLogin(new Date(),"102");
        userLoginStats.recordLogout(new Date(System.currentTimeMillis()+1000*60*25),"102");
        
        userLoginStats.recordLogin(new Date(),"103");
        userLoginStats.recordLogout(new Date(System.currentTimeMillis()+1000*60*15),"103");
        
        userLoginStats.recordLogin(new Date(),"104");
        userLoginStats.recordLogout(new Date(System.currentTimeMillis()+1000*60*8),"104");
        
        System.out.println("");
        System.out.println("RECORDED LOGIN STATISTICS FOR THE CURRENT USER ARE AS FOLLOWS: ");
        System.out.println();
        
        userLoginStats.listAllUserSessions().forEach((curSession)->{
            
            System.out.println("Session Login: "+curSession.getLoginTime()+
                              " Session Logout: "+curSession.getLogoutTime()+
                              " Session Dutation: "+curSession.getSessionDuration()/1000/60+" minutes.");
            System.out.println();
        
        
        });
        
        
        //THEN FINALLY TO PROVE WE HAVE DYNAMICALLY DEFINED CREATED THESE METHODS AT RUNTIME **WITHOUT** 
        //ALTERING THE ORIGINAL "USER" CLASS IN ANYWAY WE PRINT ITS PUBLIC INTERFACE METHODS TO THE
        //CONSOLE. YOU WILL NOTE THAT THE LOGIN STATISTIC METHODS WE HAVE JUST EXECUTED ABOVE ARE NOT AMONG THEM.
        System.out.println("");
        System.out.println("THE USER CLASS METHODS ARE: ");
        System.out.println("");
        for(Method m : User.class.getMethods())
            System.out.println(m.getName());
            
        
    }
    
}
