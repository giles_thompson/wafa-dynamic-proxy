/*
 * Copyright 2019 Giles Thompson.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package uk.gilesthompson.projects.wafa.utils.proxy.utilities;

/**
 * A collection of static proxy utility methods.
 * @author giles
 */
public final class PUtil {
    
    public static Integer zeroIfNull(Integer anInteger){
        
        if(anInteger == null)
            return 0;
        
        return anInteger;
    }
    
    public static Float zeroIfNull(Float aFloat){
        
        if(aFloat == null)
            return new Float(0);
        
        return aFloat;
    }
    
    public static Long zeroIfNull(Long aLong){
        
        if(aLong == null)
            return new Long(0);
        
        return aLong;
    }
    
    public static Double zeroIfNull(Double aDouble){
        
        if(aDouble == null)
            return new Double(0);
        
        return aDouble;
    }
    
    public static Byte zeroIfNull(Byte aByte){
        
        if(aByte == null)
            return new Byte((byte)0);
        
        return aByte;
    }
    
    public static Short zeroIfNull(Short aShort){
        
        if(aShort == null)
            return new Short((short)0);
        
        return aShort;
    }
    
    public static Boolean zeroIfNull(Boolean aBoolean){
        
        if(aBoolean == null)
            return new Boolean(false);
        
        return aBoolean;
    }
    
    public static Character zeroIfNull(Character aCharacter){
        
        if(aCharacter == null)
            return new Character('0');
        
        return aCharacter;
    }
    
    
    public static Object[] getParamValuesAsObjectArray(ParamValue[] paramValues){
        
        Object[] paramObjArray = new Object[paramValues.length];
        
        for(int i = 0; i < paramValues.length; i++)
            if(paramValues[i].getValueType() == ParamValue.VALUE_TYPE.OBJECT)
                paramObjArray[i] = paramValues[i].getOvalue();
            else if(paramValues[i].getValueType() == ParamValue.VALUE_TYPE.BOOLEAN)
                paramObjArray[i] = paramValues[i].isZvalue();
            else if(paramValues[i].getValueType() == ParamValue.VALUE_TYPE.BYTE)
                paramObjArray[i] = paramValues[i].getBvalue();
            else if(paramValues[i].getValueType() == ParamValue.VALUE_TYPE.CHAR)
                paramObjArray[i] = paramValues[i].getCvalue();
            else if(paramValues[i].getValueType() == ParamValue.VALUE_TYPE.DOUBLE)
                paramObjArray[i] = paramValues[i].getDvalue();
            else if(paramValues[i].getValueType() == ParamValue.VALUE_TYPE.FLOAT)
                paramObjArray[i] = paramValues[i].getFvalue();
            else if(paramValues[i].getValueType() == ParamValue.VALUE_TYPE.INT)
                paramObjArray[i] = paramValues[i].getIvalue();
             else if(paramValues[i].getValueType() == ParamValue.VALUE_TYPE.LONG)
                paramObjArray[i] = paramValues[i].getLvalue();
             else if(paramValues[i].getValueType() == ParamValue.VALUE_TYPE.SHORT)
                paramObjArray[i] = paramValues[i].getSvalue();
        
        
        
        
        return paramObjArray;
    }
    
    
}
