/*
 * Copyright 2019 Giles Thompson.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package uk.gilesthompson.projects.wafa.utils.proxy.gen;

import java.io.IOException;
import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassWriter;

/**
 *
 * @author giles
 */
public abstract class ProxyByteCodeGenerator {
    
    protected final String fullyQualifiedClassName;
    
    protected final Class[] additionalInterfaces;
    
    public static ProxyByteCodeGenerator create(String fullyQualifiedClassName, Class[] additionalInterfaces){
        
        return new DefaultProxyByteCodeGenerator(fullyQualifiedClassName,additionalInterfaces);
    }
    
    /**
     * 
     * @param fullyQualifiedClassName The fully qualified class name of the class to generate a proxy for.
     * @param additionalInterfaces An optional array of additional interfaces that the proxy must implement. 
     */
    public ProxyByteCodeGenerator(String fullyQualifiedClassName,Class[] additionalInterfaces){
        
        this.fullyQualifiedClassName = fullyQualifiedClassName;
        this.additionalInterfaces = additionalInterfaces;
    }
    
    /**Generates and return a DynamicProxy implementation for the specified class
     * and additional interfaces (where specified) as an array of bytes representing 
     * the DynamicProxy's bytecode.
     * @return byte[] The DynamicProxy Bytecode
     * @throws java.io.IOException*/
    public abstract byte[] generate() throws IOException;
    
    
    /** Default ProxyByteCodeGenerator implementation. */
    public static final class DefaultProxyByteCodeGenerator extends ProxyByteCodeGenerator{
    
        
        public DefaultProxyByteCodeGenerator(String fullyQualifiedClassName, Class[] additionalInterfaces) {
            
            super(fullyQualifiedClassName, additionalInterfaces);
        }

        @Override
        public final byte[] generate() throws IOException {
            
           ClassReader cr = new ClassReader(this.fullyQualifiedClassName);
           ClassWriter cw = new ClassWriter(ClassWriter.COMPUTE_FRAMES);
           ProxyGenClassVisitor pgcv = new ProxyGenClassVisitor(cw,this.additionalInterfaces);
           cr.accept(pgcv,0);
           byte[] classBytes = cw.toByteArray();
           return classBytes;
        }
    
    
}
    
}
