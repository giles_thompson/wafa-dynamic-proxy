/*
 * Copyright 2019 Giles Thompson.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package uk.gilesthompson.projects.wafa.utils.proxy.tests;

import java.lang.reflect.Method;
import uk.gilesthompson.projects.wafa.utils.proxy.utilities.AbstractProxyHandler;
import uk.gilesthompson.projects.wafa.utils.proxy.utilities.ParamValue;

/**
 * Here we extend the AbstractProxyHandler which will provide us with the
 * default implementation for all proxied methods we DO NOT wish to augment
 * merely passing the calls to these methods through to the underling source class.
 * @author giles
 */
public class AnotherTestProxyHandler extends AbstractProxyHandler  {

    @Override
    public Object handleMethodInvocation(Object proxiedObj, Method method,ParamValue[] paramValues) {
        
               if(method.getName().startsWith("get")){
                
                 long startTime = System.nanoTime();
            
                 Object result = super.handleMethodInvocation(proxiedObj,method,paramValues);
            
                 long endTime = System.nanoTime();
            
                 System.out.println("Execution of method: "+method.getName()+" tooK: "+((endTime-startTime)/1000)+" milliseconds");
                  
                 //Change String return values to augmented to clearly demonstrate 
                 //that the original methods execution path has been dyamically intercpeted.
                 if(method.getReturnType() == String.class)
                       result = "METHOD MODIFIED!!";

                 return result;
            }
            else //otherwise delegate to our super class to pass-through method to the Source Class
              return super.handleMethodInvocation(proxiedObj,method,paramValues); 
    }
  
}
