#WAFA DynamicProxy   
   

##Overview
***
WAFA (Webservices Application Framework API) , pronounced "wafer", is a super light-weight framework for building 
database-aware webservices in Java. It is lightweight both in respect to its dependency requirements 
(in contrast to similar frameworks like Spring) and its level of abstraction over the built in database 
primitives provided by JDBC. Part of the WAFA suite of utilities, **DynamicProxy is 
a small but powerful tool that is able to quickly and efficiently generate, binary interface 
compatible, proxies of any arbitrarily specified, compiled Java classes at runtime.** Whilst the library
may indeed be of some use in a wide variety of software development and engineering domains, the most common 
use case for tools of this nature is to facilitate the dynamic interception, instrumentation and/or 
augmentation of compiled class method execution at runtime.

##Wait... isn't there a built-in Java Dynamic Proxy Mechanism 
***
Yep, there sure is. But it is of extremely limited use in all but a few practical usecases (indeed this
library would likely not exist were this not to be the case) this is principally owing to the fact
that **the Dynamic Proxy provided by the good folks over at Oracle is only able to proxy INTERFACE types
which thus mandates that developers have or are able to create interfaces for every class they wish to proxy
ahead of time!** This renders the component practically useless for most compiled class 
instrumentation and augmentation tasks since typically the developer will:

1)**Not actually have access to the class sources;** indeed compiled class method augmentation is often chosen
  as solution specifically BECAUSE there is a requirement to change the behavior of one or more classes 
  that the developer has no source access to and thus therefore unable to modify to implement the
  requisite interfaces.

2)**Have the sources files for the compiled classes but find it to be an impractical endeavor to create interfaces
  for ALL classes they wish to proxy a head of time.** This is especially true for instrumentation usecases where 
  its not untypical for EVERY class in an application to require some augmentation.

3)**Not even be aware of the class types required to be instrumented/augmented until runtime.** This is especially true
  for developers (like myself :) ) that create Frameworks intended for use by third parties. Typically these
  frameworks... well... work on an IOC (Inversion-Of-Control) basis and are required to instrument/augment
  any and all arbitrarily specified thrid-party developer classes at runtime.


##Why WAFA DynamicProxy?
***
Well, like most software development tools, DynamicProxy was born out of need that was not currently being
met by existing, publicly available solutions. **DynamicProxy is able to quickly and easily 
generate proxies from CONCRETE classes as well as one or more additional interfaces at runtime.** 
Which, thus, means it is able to serve as a proxy for ANY class **without requiring that interfaces be defined 
for these classes ahead of time.** The resulting DynamicProxy instance may then **be freely cast to the 
concrete type or indeed to any of the additional interface types** (where specified) irrespective of which
ClassLoader loaded the classes and where the ClassLoader appears in the JVM Hierarchy. Please view the 
**Basic Usage Example** section for clarification of how this works. The only notable exception are classes declared 
as final; such classes may still be proxied but must be so as abstract interface types. 

##Getting Started
***   
Whilst you are, of course, free to **clone** and **build** this repository's sources yourself, do note
that this is a **maven** based project and thus the quickest way to get started is simply to alter
your **project pom file** as follows:    
    
 
First you need to add an **additional reposistory defintion** for the project: 
```XML

<repositories>
        
        <repository>
            
            <snapshots>
                <enabled>true</enabled>
                <updatePolicy>always</updatePolicy>
                <checksumPolicy>warn</checksumPolicy>
            </snapshots>
            
            <id>GT-PROJECTS-HUB</id>
            <name>GT-PROJECTS-HUB</name>
            <url>http://targethub.org/maven2</url>
            <layout>default</layout>
        </repository>
        
        
    </repositories>


```    
Next you actually add the **wafa-dynamic-proxy dependency** to your project to pom:    
```XML

<dependencies>
        
        <dependency>
            
            <groupId>uk.gilesthompson.projects.wafa</groupId>
            <artifactId>wafa-dynamic-proxy</artifactId>
            <version>1.01</version>
            
        </dependency>
        
    </dependencies>


```

Thats all there is to it!! Simply **compile** and **rebuild** your project and you'll be good to go!!    
    



##Basic Usage Example.
***

###Prequisites
In order to use DynamicProxy the following resources are required:

 *  An instance of the **Source Class** that is required to be proxied.

 *  A **ProxyHandler**, this is where you define what actions to take when calls are made to the proxied methods.

 *  An **optional** list of **Additional Interfaces** that the proxy is required to implement. This is useful
    for dynamically adding additional methods to existing classes at runtime. See the **Advanced Usage** section
    for more information pertaining to **Additional Interfaces**; they will not be covered in this basic example.

1)We begin with Our **Source Class**:
Just a simple User model class with a few getters and setters we would like to proxy.

```JAVA

public class User { 

  private int id;
  private String firstName, surname;
  public int getId() {return id;}
  public void setId(int id) {this.id = id;}
  public String getFirstName() {return firstName;}
  public void setFirstName(String firstName) {this.firstName = firstName;}
  public String getSurname() {return surname;}
  public void setSurname(String surname) {this.surname = surname;}

}

```

2)Next we define our **ProxyHandler** Implementation.
Here there are **two** options, which you elect to use will very much be dependent on your specific use case
but may be broadly catergorised as follows: 

*   Where there is a requirement to only instrument/augment a **subset** of your **Source Class** methods 
    then the **AbstractProxyHandler Class** should be **extended** as it provides built-in operations to more 
    easily **pass-through**  calls to the remaining methods to the underlying **Source Class** unchanged.

*   Conversely, where you would like to augment ALL of your **Source Class** methods or 
    are **adding additional methods to the SourceClass** then the **ProxyHandler Interface** should be 
    implemented directly. 


In both cases only a SINGLE method is required to be overriden namely the **handleMethodInvocation** method.
For the purposes of this example, we **extend** the **AbstractProxyHandler** as we would only like to augment 
a subset of the methods defined in our **Source Class**, (just "getter" methods) calls to other methods
will be passed through unchanged.

```JAVA
public class ExampleProxyHandler extends AbstractProxyHandler {

    @Override
    /**@param proxiedObj A Reference to the underlying SourceClass object being proxied
                         used for delegating to or obtaining data from the Source Class.

       @param method     The method that was called on the Proxy  

       @param paramValues An array of ParamValues representing each parameter that was passed
                          in to the proxied method. These may ofcourse be logged for debugging
                          purposes, augmented, or complete replaced depending on the requirements
                          of the application.
                          
    */
    public Object handleMethodInvocation(Object proxiedObj, Method method,ParamValue[] paramValues) {

          /** 
              Here we log the time taken to execute "getter" methods in
              our Source Class and we CHANGE THEIR RETURN VALUES to "Augmented"
              where the method returns a String. All other methods are
              passed-through unchanged.
           */
            if(method.getName().startsWith("get")){
                
                 long startTime = System.nanoTime();
            
                 Object result = super.handleMethodInvocation(proxiedObj,method,paramValues);
            
                 long endTime = System.nanoTime();
            
                 System.out.println("Execution of method: "+method.getName()+" tooK: "+((endTime-startTime)/1000)+" milliseconds");
                  
                 //Change String return values to augmented to clearly demonstrate 
                 //that the original methods execution path has been dyamically intercpeted.
                 if(method.getReturnType() == String.class)
                       result = "METHOD MODIFIED!!";

                 return result;
            }
            else //otherwise delegate to our super class to pass-through method to the Source Class
              return super.handleMethodInvocation(proxiedObj,method,paramValues); 
                  
           
    }
}

```

###Using WAFA DynamicProxy
Using the DynamicProxy component couldn't be simpler. An instance may be obtained
from the **DynamicProxyFactory** via the **newDynamicProxy** method, to which we must 
provide a reference to the **Source Class** instance and **ProxyHandler** we created above.
These details are used to instantly generate and initialise a brand new DynamicProxy instance.
The generated class is completely transient and destroyed once the JVM process exits.
```JAVA

   /**
      Instantiate a new DynamicProxyFactory and call its "newDynamicProxy"
      method to obtain reference to a new DynamicProxy. The User and ProxyHandler
      classes created above are passed in as parameters.
   */
   DynamicProxyFactory dynamicProxyFactory = DynamicProxyFactory.create();
   DynamicProxy dynamicProxy = dynamicProxyFactory.newDynamicProxy(new User(),
                                                                   new ExampleProxyHandler());
  
   /**Next the simplest way to use the DynamicProxy is to cast it to the type we
      wish to call methods upon, User in this case. (Although Reflection may also be used)*/
   User user (User)dynamicProxy;

   /**Then finally to demonstrate the specified methods execution paths are being 
      intercepted by the proxy we may do the following.*/
      user.setId(1);
      user.setFirstName("Giles");
      user.setSurname("Thompson");
      System.out.println("getId value: "+user.getId());
      System.out.println("getFirstName value: "+user.getFirstName());
      System.out.println("getSurname value: "+user.getSurname());

```

Compiling and executing the preceeding code will yield the following console output:

```
Execution of method: getId tooK: 71 milliseconds
getId value: 1
Execution of method: getFirstName tooK: 64 milliseconds
getFirstName value: METHOD MODIFIED!!
Execution of method: getSurname tooK: 62 milliseconds
getSurname value: METHOD MODIFIED!!

```
The output is **exactly** as we expect in that the following conditions
(defined in our **ProxyHandler**) hold true:

 *   All "getter" methods have been instrumented to print their respective execution
     times in milliseconds.

 *   The "getter" methods that return String values (getFirstName and getSurname) have 
     been augmented as expected to return the literals: "METHOD MODIFIED" irrespective 
     of the values the user setted th corresponding fields to via the "setter" methods.
     (i.e Giles and Thompson) respectfully.

 *   All other methods (i.e our "setter" methods) have been passed-through unchanged
     to the **Source Class** exactly as we defined in our handler.

Whilst this example was **actually designed** to be contrived to some extent, it does hopefully 
serve to demonstrate just how simple it is to use DynamicProxy to begin modifying your compiled
classes at runtime and we are really just scratching the surface here; please see the Advanced Usage 
section for a real-world example of how DynamicProxy can be used to dynamically change the **structure**
of compiled classes by allowing for the, near instant, addition of **entirely new methods** and associated 
fields at runtime. 


##Advanced Usage Example
***

###Example Overview
During the perpetual development lifecycle of any mission-critical software application or MVP its common
for there to be a requirement to prototype and test new features before they are fully rolled out
and promoted to be permanent additions to the core code base. In this era of Big Data it is also
not uncommon for services to solely exist to collect, collate and derive some degree intelligence or
insights from the use of and data produced by other constituent services in an application. These 
Meta Data related services may be developed to collect data over a certain finite amount of time 
(i.e for a specific analysis endeavor) or may, after being sufficiently tested, remain in place indefinitely. 


###Example Requirements
For the purposes of this example we will consider a very common use case in line with the former. That is, we would like
to be able to collect meta-data relating to certain entities in an application for a finite amount of time
(i.e for the purposes of completing a report or generating a snapshot of data points for further analysis)
More formally the full requirements are:

1)We would like to collect login and session based statistics related to all users in our system in order to gauge the times of day our application is used most,which group of users are the most frequent users,how long, on average, each user
remains logged in,etc. In order to do this **extra fields and methods must be added to the User model class**
introduced in the Basic Example above, to collect this extra data.

2)The exercise should be undertaken in a way that causes the minimum amount of disruption to the running
 and maintenance of the core application and all modifications made to components for the
 purposes of this analysis MUST be revoked once the exercise is concluded.



###Example Solution
We begin by returning to the **User** class introduced in the Basic Example above as you can see below the modifications
we need to make to the class have been highlighted:

```
public class User {
    
  private int id;
  private String firstName, surname; 
  𝙥𝙧𝙞𝙫𝙖𝙩𝙚 𝙛𝙞𝙣𝙖𝙡 𝙈𝙖𝙥<𝙎𝙩𝙧𝙞𝙣𝙜,𝙐𝙨𝙚𝙧𝙎𝙚𝙨𝙨𝙞𝙤𝙣> 𝙪𝙨𝙚𝙧𝙎𝙚𝙨𝙨𝙞𝙤𝙣𝙨 = 𝙣𝙚𝙬 𝙃𝙖𝙨𝙝𝙈𝙖𝙥<>();
  public int getId() {return id;}
  public void setId(int id) {this.id = id;}
  public String getFirstName() {return firstName;}
  public void setFirstName(String firstName) {this.firstName = firstName;}
  public String getSurname() {return surname;}
  public void setSurname(String surname) {this.surname = surname;}

  𝙥𝙪𝙗𝙡𝙞𝙘 𝙫𝙤𝙞𝙙 𝙧𝙚𝙘𝙤𝙧𝙙𝙇𝙤𝙜𝙞𝙣(𝘿𝙖𝙩𝙚 𝙩𝙞𝙢𝙚,𝙎𝙩𝙧𝙞𝙣𝙜 𝙨𝙚𝙨𝙨𝙞𝙤𝙣𝙄𝙙){

      𝙩𝙝𝙞𝙨.𝙪𝙨𝙚𝙧𝙎𝙚𝙨𝙨𝙞𝙤𝙣𝙨.𝙥𝙪𝙩(𝙨𝙚𝙨𝙨𝙞𝙤𝙣𝙄𝙙,𝙣𝙚𝙬 𝙐𝙨𝙚𝙧𝙎𝙚𝙨𝙞𝙤𝙣());

  }
  𝙥𝙪𝙗𝙡𝙞𝙘 𝙫𝙤𝙞𝙙 𝙧𝙚𝙘𝙤𝙧𝙙𝙇𝙤𝙜𝙤𝙪𝙩(𝘿𝙖𝙩𝙚 𝙩𝙞𝙢𝙚,𝙎𝙩𝙧𝙞𝙣𝙜 𝙨𝙚𝙨𝙨𝙞𝙤𝙣𝙄𝙙){

      𝙩𝙝𝙞𝙨.𝙪𝙨𝙚𝙧𝙎𝙚𝙨𝙨𝙞𝙤𝙣𝙨.𝙜𝙚𝙩("𝙨𝙚𝙨𝙨𝙞𝙤𝙣𝙄𝙙")
                       .𝙨𝙚𝙩𝙇𝙤𝙜𝙤𝙪𝙩𝙏𝙞𝙢𝙚(𝙩𝙞𝙢𝙚);
  }
  𝙥𝙪𝙗𝙡𝙞𝙘 𝘾𝙤𝙡𝙡𝙚𝙘𝙩𝙞𝙤𝙣<𝙐𝙨𝙚𝙧𝙎𝙚𝙨𝙨𝙞𝙤𝙣> 𝙡𝙞𝙨𝙩𝘼𝙡𝙡𝙐𝙨𝙚𝙧𝙎𝙚𝙨𝙨𝙞𝙤𝙣𝙨(){

      𝙧𝙚𝙩𝙪𝙧𝙣 𝙩𝙝𝙞𝙨.𝙪𝙨𝙚𝙧𝙎𝙚𝙨𝙨𝙞𝙤𝙣𝙨.𝙫𝙖𝙡𝙪𝙚𝙨();

  }
  
}

```


But... before we race ahead to get to work on refactoring our **User** class (as detailed above).   
Lets pause for a moment and consider the requirements,as it turns out A solution based on 
**WAFA DynamicProxy** would be **perfect** for this use case It would allow for the following:

*   The dynamic addition of extra methods and fields to collect this statistical data **WITHOUT** modification
    of the original **User** class.

*   Specifically BECAUSE **DynamicProxy** is able to proxy CONCRETE types rather than just INTERFACE types 
    (like the built in Java Proxy Mechanism), **the proxied versions of classes may be freely used 
    in place of their original counterparts** for the full duration of the data collection exercise. Thus there 
    will be **absolutely no disruption** to other services in the application that depend on the **User** class.


*   Zero need for code revocation and refactoring once the exercise is complete, since as detailed above, using
    **DynamicProxy** means we don't actually have to modify the original **User** class to collect this 
    extra login/session based statistical data.


So lets take a look at what a **DynamicProxy** centric solution might look like, we will need to define the following:   
1)A **UserLoginStats** interface.   
2)A **User ProxyHandler**   


We begin by defining the **UserLoginStats** interface this will include all methods we would like to add to the **User** class to 
collect the extra Login and Session based data, this may look as follows:

```JAVA

public interface UserLoginStats {
    public void recordLogin(Date time,String sessionId);
    public void recordLogout(Date time,String sessionId);
    public Collection<UserSession> listAllUserSessions();

    /** 
        Sub Interface to be used by our ProxyHandler
        methods defined here will NOT be implemented by the 
        resulting DynamicProxy like those above. 
     */
    public interface UserSession{
        public Date getLoginTime();
        public Date getLogoutTime();
        public long getSessionDuration();
        public void setLoginTime(Date aDate);
        public void setLogoutTime(Date aDate);
    }
}


```




Next we define a **ProxyHandler** for the **User** DynamicProxy in much the same way as we did in the basic example above, you 
will note though that this time our **handleMethodInvocation** implementation passses all non-login related methods through to 
the original **User** class that we are proxying. Additionally, conditionals have now been added to 
specifically handle login related data as well as fields to collect this data:

```JAVA

public class UserProxyHandler extends AbstractProxyHandler {
    
    //Extra map field to store login centric information. Here we store
    //instances of UserSession to the map under the login id.
    private final Map<String,UserSession> userSessions;

    public UserProxyHandler() {
        
        this.userSessions = new HashMap<>();
    }
    
    @Override
    public Object handleMethodInvocation(Object proxiedObj, Method method, ParamValue[] parameterValues) {
        
        /**
           if the method exists in the original concrete "User" class then its NOT related to the login 
           related methods defined in our UserLoginStats interface thus we simply pass the call through,
           otherwise the method MUST be explictly handled here.The "methodExistsInConcreteClass" method 
           provided by our super class will determine this for us.
         */
        if(methodExistsInConcreteClass(proxiedObj, method))
            return super.handleMethodInvocation(proxiedObj, method, parameterValues); 
        else{
            
            switch (method.getName()) {
                case "recordLogin":
                    Date date = (Date)parameterValues[0].getOvalue(); //get the first parameter (i,e the date)
                    String sessId = (String)parameterValues[1].getOvalue(); //get the second param (our session id)
                    UserSessionImpl us = new UserSessionImpl(); //create a UserSession instance and store it to our map.
                    us.setLoginTime(date);
                    synchronized(this.userSessions){this.userSessions.put(sessId, us);}
                    break;
                case "recordLogout":
                    Date  logoutDate = (Date)parameterValues[0].getOvalue(); //get the first parameter (i,e the date)
                    String sId = (String)parameterValues[1].getOvalue(); //get the second param (our session id)
                    synchronized(this.userSessions){this.userSessions.get(sId) //lookup Session and set logout time.
                                                                     .setLogoutTime(logoutDate);}
                   
                    break;
                case "listAllUserSessions":
                    synchronized(this.userSessions){return this.userSessions.values();}
                default:
                    break;
            }
            
            return null;
        }
    }
    
    /**
        Quick implementation of the UserSession Sub Interface defined in the
        UserLoginStats interface. 
     */
     public class UserSessionImpl implements UserLoginStats.UserSession{
        private Date loginTime;
        private Date logoutTime;
        private long sessionDuration;

        @Override
        public Date getLoginTime() {return this.loginTime;}
        @Override
        public Date getLogoutTime() {return this.logoutTime;}
        @Override
        public long getSessionDuration() {return this.sessionDuration;}
        @Override
        public void setLoginTime(Date aDate) {this.loginTime = aDate;}
        @Override
        public void setLogoutTime(Date aDate) {this.logoutTime  = aDate; 
                                               this.sessionDuration = (this.logoutTime.getTime()-this.loginTime.getTime());}
       
         
     }
}


```


Great!! At this point we may instantiate our **User** DynamicProxy and begin using it to collect this
extra login data; the full code example of doing this would be as follows:

```JAVA

public class AdvancedExample {
    
    
    
    public static void main(String[] args) throws ProxyGenerationException{
        
        /** 
            Create our factory and use it to obtain a new reference to a new DynamicProxy instance; 
            we take care to pass in the concrete "User" class we wish to proxy, Our "ProxyHandler"
            AND this time a single "additional interface" namely our "UserLoginStats" Interface 
            The resulting DynamicProxy will now implement the public interface of BOTH of these 
            components and pass all calls through to both to our handler
            NOTE: Any amount of "additional interfaces" may be specified to add further methods
            *     to the public interface of the DynamicProxy.
       */
        
        DynamicProxyFactory dynamicProxyFactory = DynamicProxyFactory.create();
        DynamicProxy dynamicProxy = dynamicProxyFactory.newDynamicProxy(new User(), 
                                                                        new UserProxyHandler(), 
                                                                        UserLoginStats.class);
        
        
        
        //1)BEFORE WE BEGIN WORKING WITH OUR PROXY LETS PRINT SOME OF ITS DETAILS TO THE CONSOLE.
        System.out.println("THE DYNAMICPROXY INSTANCE DETAILS ARE AS FOLLOWS: ");
        System.out.println("");
        System.out.println("The DynamicProxy internal class name is: "+dynamicProxy.getDynamicProxyClassName());
        System.out.println("The Source class its proxying for is:    "+dynamicProxy.getSourceClassName());
        System.out.println("Additional interfaces it implements are: "+Arrays.toString(dynamicProxy.getAdditionalInterfaces()));
        
        
        //2)WE MAY CAST THE DYNAMICPROXY TO A USER TO WORK WITH IT AS A USER CLASS AS WE DID IN THE BASIC EXAMPLE
        //ALL CALLS ARE PASSED THROUGH TO THE UNDERLYING INSTANCE AND IT IS INDISTINGUISHABLE FROM THE ORIGINAL CLASS TO THE REST OF THE APP
        System.out.println("");
        System.out.println("USER DETAILS ARE AS FOLLOWS: ");
        System.out.println("");
        User user = (User)dynamicProxy;
        user.setId(1);
        user.setFirstName("Giles");
        user.setSurname("Thompson");
        System.out.println("User id value is: "+user.getId());
        System.out.println("User first name value is: "+user.getFirstName());
        System.out.println("User surname value is: "+user.getSurname());
        
        
        //3)OK SO FAR SO GOOD, BUT THE WHOLE POINT OF THIS EXERCISE WAS TO ADD **EXTRA** OPERATIONS
        //  AT RUNTIME. WHICH WE HAVE DONE BY SPECIFYING AN "ADDITIONAL INTERFACE" AND PROVIDING AN
        //  IMPLEMENTATION FOR ITS METHODS IN OUR PROXYHANDLER. ACCESSING THESE EXTA METHODS
        //  COULDNT BE SIMPLER WE JUST CAST THE **SAME** DYNAMICPROXY INSTANCE TO THAT INTERFACE...
        UserLoginStats userLoginStats = (UserLoginStats) dynamicProxy;
        
        //simulate a bunch of logins and logouts...
        userLoginStats.recordLogin(new Date(),"101");
        userLoginStats.recordLogout(new Date(System.currentTimeMillis()+1000*60*30),"101");
        
        userLoginStats.recordLogin(new Date(),"102");
        userLoginStats.recordLogout(new Date(System.currentTimeMillis()+1000*60*25),"102");
        
        userLoginStats.recordLogin(new Date(),"103");
        userLoginStats.recordLogout(new Date(System.currentTimeMillis()+1000*60*15),"103");
        
        userLoginStats.recordLogin(new Date(),"104");
        userLoginStats.recordLogout(new Date(System.currentTimeMillis()+1000*60*8),"104");
        
        System.out.println("");
        System.out.println("RECORDED LOGIN STATISTICS FOR THE CURRENT USER ARE AS FOLLOWS: ");
        System.out.println();
        
        userLoginStats.listAllUserSessions().forEach((curSession)->{
            
            System.out.println("Session Login: "+curSession.getLoginTime()+
                              " Session Logout: "+curSession.getLogoutTime()+
                              " Session Dutation: "+curSession.getSessionDuration()/1000/60+" minutes.");
            System.out.println();
        
        
        });
        
        
        //THEN FINALLY TO PROVE WE HAVE DYNAMICALLY DEFINED CREATED THESE METHODS AT RUNTIME **WITHOUT** 
        //ALTERING THE ORIGINAL "USER" CLASS IN ANYWAY WE PRINT ITS PUBLIC INTERFACE METHODS TO THE
        //CONSOLE. YOU WILL NOTE THAT THE LOGIN STATISTIC METHODS WE HAVE JUST EXECUTED ABOVE ARE NOT AMONG THEM.
        System.out.println("");
        System.out.println("THE USER CLASS METHODS ARE: ");
        System.out.println("");
        for(Method m : User.class.getMethods())
            System.out.println(m.getName());
            
        
    }
    
}


```









Compiling and executing the above code segment will yield the following console output:

```
THE DYNAMICPROXY INSTANCE DETAILS ARE AS FOLLOWS: 

The DynamicProxy internal class name is: uk/gilesthompson/projects/wafa/utils/proxy/tests/UserProxy
The Source class its proxying for is:    uk/gilesthompson/projects/wafa/utils/proxy/tests/User
Additional interfaces it implements are: [interface uk.gilesthompson.projects.wafa.utils.proxy.tests.UserLoginStats]

USER DETAILS ARE AS FOLLOWS: 

User id value is: 1
User first name value is: Giles
User surname value is: Thompson

RECORDED LOGIN STATISTICS FOR THE CURRENT USER ARE AS FOLLOWS: 

Session Login: Mon Apr 29 05:03:41 BST 2019 Session Logout: Mon Apr 29 05:33:41 BST 2019 Session Dutation: 30 minutes.

Session Login: Mon Apr 29 05:03:41 BST 2019 Session Logout: Mon Apr 29 05:28:41 BST 2019 Session Dutation: 25 minutes.

Session Login: Mon Apr 29 05:03:41 BST 2019 Session Logout: Mon Apr 29 05:18:41 BST 2019 Session Dutation: 15 minutes.

Session Login: Mon Apr 29 05:03:41 BST 2019 Session Logout: Mon Apr 29 05:11:41 BST 2019 Session Dutation: 8 minutes.


THE USER CLASS METHODS ARE: 

getId
setId
setFirstName
setSurname
getFirstName
getSurname
wait
wait
wait
equals
toString
hashCode
getClass
notify
notifyAll

```


Again, this is **exactly** as we expect on account of the fact that:

*    We have been able to dynamically add and utilise extra methods to create
     store and access login/session statistical data as required.

*    The User DynamicProxy is able to be used in place of the original **User** class in all other respects.
     i.e we can call all User specific methods on it (setFirstName etc) in exactly th same way.


*    Zero changes have been made to the existing **User** classes to facilitate
     the collection of this extra data as evidenced by our print out of the **User** classes methods.




##Contributing
***
Suggestions are welcome, please email me at **gilesthompson@hotmail.co.uk**
to discuss first.



##Authors
***
**Name:**    Giles Thompson   
**Email:**   gilest@hotmail.co.uk.



##License
***
[Apache 2.0 License](http://www.apache.org/licenses/LICENSE-2.0)



##Acknowlegements
***

*   Huge thank you to the folks over at the OW2 Consortium for their 
    ongoing development of the ASM project which is one of this projects
    dependencies. See: [ASM License](https://asm.ow2.io/license.html)






